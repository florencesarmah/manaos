package monnaie;

/* Created with love by Florence Sarmah on 15.10.2020 */

import org.junit.Assert;
import org.junit.Test;

public class MonnaieBuilderTest
{
	@Test
	public void should_returnNull_when_sumIsImpossible()
	{
		Monnaie result = MonnaieBuilder.monnaieOptimale(3);
		Assert.assertNull(result);
	}

	@Test
	public void should_returnNull_when_sumIsSmallestThanTheMinimalPiece()
	{
		Monnaie result = MonnaieBuilder.monnaieOptimale(1);
		Assert.assertNull(result);
	}

	@Test
	public void should_returnCorrectMonnaie_when_sumIsTheSmallestPossible()
	{
		Monnaie result = MonnaieBuilder.monnaieOptimale(2);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.getPiece2());
		Assert.assertEquals(0, result.getBillet5());
		Assert.assertEquals(0, result.getBillet10());
	}

	@Test
	public void should_returnCorrectMonnaie_when_sumIsTheLargestPossible()
	{
		Monnaie result = MonnaieBuilder.monnaieOptimale(9223372036854775807L);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.getPiece2());
		Assert.assertEquals(1, result.getBillet5());
		Assert.assertEquals(922337203685477580L, result.getBillet10());
	}
}
