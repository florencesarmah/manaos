package monnaie;

/* Created with love by Florence Sarmah on 15.10.2020 */

import org.junit.Assert;
import org.junit.Test;

public class MonnaieTest
{
	@Test
	public void should_createAMonnaieObject_when_allParametersAreMoreThan0()
	{
		Monnaie monnaie = new Monnaie(1, 2, 3);
		Assert.assertEquals(1, monnaie.getPiece2());
		Assert.assertEquals(2, monnaie.getBillet5());
		Assert.assertEquals(3, monnaie.getBillet10());
	}

	@Test
	public void should_createAMonnaieObject_when_theSmallestNumberPossibleIsGiven()
	{
		Monnaie monnaie = new Monnaie(1, 0, 0);
		Assert.assertEquals(1, monnaie.getPiece2());
		Assert.assertEquals(0, monnaie.getBillet5());
		Assert.assertEquals(0, monnaie.getBillet10());
	}

	@Test
	public void should_createAMonnaieObject_when_theLargestNumberPossibleIsGiven()
	{
		Monnaie monnaie = new Monnaie(1, 1, 922337203685477580L);
		Assert.assertEquals(1, monnaie.getPiece2());
		Assert.assertEquals(1, monnaie.getBillet5());
		Assert.assertEquals(922337203685477580L, monnaie.getBillet10());
	}
}
