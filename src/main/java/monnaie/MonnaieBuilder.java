package monnaie;

/* Created with love by Florence Sarmah on 15.10.2020 */

public class MonnaieBuilder
{
	public static Monnaie monnaieOptimale(long s)
	{
		long numberOfBillet10 = s / 10;
		int remainderOf10 = Math.toIntExact(s % 10);    //It will never be more than 9
		int numberOfBillet5 = remainderOf10 / 5;
		int remainderOf5 = remainderOf10 % 5;
		int numberOfPiece2 = remainderOf5 / 2;
		int remainderOf2 = remainderOf5 % 2;

		if(remainderOf2 != 0)   //It means that the sum could not be completed
			return null;

		return new Monnaie(numberOfPiece2, numberOfBillet5, numberOfBillet10);
	}
}
