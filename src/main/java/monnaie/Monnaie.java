package monnaie;

/* Created with love by Florence Sarmah on 15.10.2020 */

public class Monnaie
{
	private final int piece2;
	private final int billet5;
	private final long billet10;

	//Since there is the billet10, the number of pieces of 2 is never more than 4.
	//For this reason, I have opted for using the int instead of long because it saves 32 bits for every transaction
	//And for the billet5, it is never more than 1. I could use boolean in this case, but for the sake of making
	//arithmetical operations easier, I will keep the billet5 as an int.
	//https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html

	public Monnaie(int piece2, int billet5, long billet10)
	{
		this.piece2 = piece2;
		this.billet5 = billet5;
		this.billet10 = billet10;
	}

	public int getPiece2()
	{
		return piece2;
	}

	public int getBillet5()
	{
		return billet5;
	}

	public long getBillet10()
	{
		return billet10;
	}
}
